//
//  ViewController.swift
//  Game1
//
//  Created by Jasmin Sudani on 13/04/23.
//

import UIKit

class ViewController: UIViewController {

  
    @IBOutlet weak var But_Game_1: UIButton!
    @IBOutlet weak var But_Game_2: UIButton!
    @IBOutlet weak var But_Game_3: UIButton!
    
    @IBOutlet weak var But_Game_4: UIButton!
    @IBOutlet weak var But_Game_5: UIButton!
    @IBOutlet weak var But_Game_6: UIButton!
    
    @IBOutlet weak var But_Game_7: UIButton!
    @IBOutlet weak var But_Game_8: UIButton!
    @IBOutlet weak var But_Game_9: UIButton!
    
    @IBOutlet weak var But_Game_10: UIButton!
    @IBOutlet weak var But_Game_11: UIButton!
    @IBOutlet weak var But_Game_12: UIButton!
    
    var image = [
    
        "1",
        "2",
        "4",
        "5",
        "6",
        "7",
        "1",
        "2",
        "4",
        "5",
        "6",
        "7"
    ]
    
    var buttuns = [UIButton]()
    
    var count = 0
    
    var click = 1
    
    var click1 = 0
    var click2 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.shuffle()
        
        buttuns.append(But_Game_1)
        buttuns.append(But_Game_2)
        buttuns.append(But_Game_3)
        buttuns.append(But_Game_4)
        buttuns.append(But_Game_5)
        buttuns.append(But_Game_6)
        buttuns.append(But_Game_7)
        buttuns.append(But_Game_8)
        buttuns.append(But_Game_9)
        buttuns.append(But_Game_10)
        buttuns.append(But_Game_11)
        buttuns.append(But_Game_12)
        
        
       
        
        
    }

    @IBAction func But_Game_Action_1(_ sender: UIButton) {
        
        if click == 1 {
            
            But_Game_1.setImage(UIImage(named: image[0]), for: .normal)
            click = 2
            click1 = 1
            
        }else if click == 2 {
            
            But_Game_1.setImage(UIImage(named: image[0]), for: .normal)
            click = 1
            click2 = 1
            
            comper()
            re()

        }
        
    }
    @IBAction func But_Game_Action_2(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_2.setImage(UIImage(named: image[1]), for: .normal)
            click = 2
            click1 = 2
        }else if click == 2 {
            
            But_Game_2.setImage(UIImage(named: image[1]), for: .normal)
            click = 1
            click2 = 2
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_3(_ sender: UIButton) {
        
        if click == 1 {
            
            But_Game_3.setImage(UIImage(named: image[2]), for: .normal)
            click = 2
            click1 = 3
        }else if click == 2 {
            
            But_Game_3.setImage(UIImage(named: image[2]), for: .normal)
            click = 1
            click2 = 3
            
            comper()
            re()

        }
    }
    
    @IBAction func But_Game_Action_4(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_4.setImage(UIImage(named: image[3]), for: .normal)
            click = 2
            click1 = 4
        }else if click == 2 {
            
            But_Game_4.setImage(UIImage(named: image[3]), for: .normal)
            click = 1
            click2 = 4
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_5(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_5.setImage(UIImage(named: image[4]), for: .normal)
            click = 2
            click1 = 5
        }else if click == 2 {
            
            But_Game_5.setImage(UIImage(named: image[4]), for: .normal)
            click = 1
            click2 = 5
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_6(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_6.setImage(UIImage(named: image[5]), for: .normal)
            click = 2
            click1 = 6
        }else if click == 2 {
            
            But_Game_6.setImage(UIImage(named: image[5]), for: .normal)
            click = 1
            click2 = 6
            
            comper()
            re()

        }
    }
    
    @IBAction func But_Game_Action_7(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_7.setImage(UIImage(named: image[6]), for: .normal)
            click = 2
            click1 = 7
        }else if click == 2 {
            
            But_Game_7.setImage(UIImage(named: image[6]), for: .normal)
            click = 1
            click2 = 7
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_8(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_8.setImage(UIImage(named: image[7]), for: .normal)
            click = 2
            click1 = 8
        }else if click == 2 {
            
            But_Game_8.setImage(UIImage(named: image[7]), for: .normal)
            click = 1
            click2 = 8
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_9(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_9.setImage(UIImage(named: image[8]), for: .normal)
            click = 2
            click1 = 9
        }else if click == 2 {
            
            But_Game_9.setImage(UIImage(named: image[8]), for: .normal)
            click = 1
            click2 = 9
            
            comper()
            re()

        }
    }
    
    @IBAction func But_Game_Action_10(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_10.setImage(UIImage(named: image[9]), for: .normal)
            click = 2
            click1 = 10
        }else if click == 2 {
            
            But_Game_10.setImage(UIImage(named: image[9]), for: .normal)
            click = 1
            click2 = 10
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_11(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_11.setImage(UIImage(named: image[10]), for: .normal)
            click = 2
            click1 = 11
        }else if click == 2 {
            
            But_Game_11.setImage(UIImage(named: image[10]), for: .normal)
            click = 1
            click2 = 11
            
            comper()
            re()

        }
    }
    @IBAction func But_Game_Action_12(_ sender: UIButton) {
        if click == 1 {
            
            But_Game_12.setImage(UIImage(named: image[11]), for: .normal)
            click = 2
            click1 = 12
        }else if click == 2 {
            
            But_Game_12.setImage(UIImage(named: image[11]), for: .normal)
            click = 1
            click2 = 12
            
            comper()
            re()

        }
    }
    
    
    func comper(){
        
        if image[click1-1] == image[click2-1]{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.70){
                
                self.buttuns[self.click1-1].alpha = 0
                self.buttuns[self.click2-1].alpha = 0
            }
            count += 1
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.70){
                
                self.buttuns[self.click1-1].setImage(UIImage(named: "8"), for: .normal)
                self.buttuns[self.click2-1].setImage(UIImage(named: "8"), for: .normal)
            }
        }
        
    }
    
    func re(){
        
        if count == 6 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.50){
                
                let x = UIAlertController(title: "done", message: "Your game is complet", preferredStyle: .alert)
                 
                let but = UIAlertAction(title: "Done", style: .default){_ in
                    
                    for i in 0...11{
                        
                        self.buttuns[i].alpha = 1
                        self.buttuns[i].setImage(UIImage(named: "8"), for: .normal)
                        self.count = 0
                        
                    }
                    
                }
                x.addAction(but)
                
                self.present(x, animated: true)
                
            }
            
        }
    }
    
}

